{
	"comment": "Specificities of IEEE Std 716-1995.",
	"$schema": "https://raw.githubusercontent.com/martinring/tmlanguage/master/tmlanguage.json",
	"name": "C/ATLAS",
	"fileTypes": [
		"as",
		"lu"
	],
	"patterns": [
		{"include": "#statement"},
		{"include": "#flags"}
	],
	"repository": {
		"statement":{
			"patterns": [
				{"include": "#variable"},
				{"include": "#parentheses"},
				{"include": "#control-statements"},
				{"include": "#operators"}
			],
			"comment": "Subsection 15.3.1. <fstatno> consists of <test number><step number>. <test number> is exactly 4 decimal digits <step number> is exactly 2 decimal digits.",		
			"begin": "^\\s((\\s{4}|\\d{4})(\\d{2}))\\s+",
			"beginCaptures":{
				"1": {"name": "constant.fstatno.atlas"},
				"2": {"name": "constant.fstatno.test-number.atlas"},
				"3": {"name": "constant.fstatno.step-number.atlas"}
			},
			"end": "(\\$\\s*\\n)",
			"endCaptures":{
				"1": {"name": "constant.statement-terminator.atlas"}
			}
		},
		"variable": {
			"comment": "Variable definition",
			"patterns": [
				{
					"comment": "Variable",
					"name": "variable.quoted.single.atlas",
					"begin": "'",
					"beginCaptures": {
						"0": {
							"name": "punctuation.definition.string.begin.atlas"
						}
					},
					"end": "'",
					"endCaptures": {
						"0": {
							"name": "punctuation.definition.string.end.atlas"
						}
					},
					"applyEndPatternLast": 1,
					"patterns": [
						{
							"name": "constant.character.escape.apostrophe.atlas",
							"match": "''"
						}
					]
				}
			]
		},
		"parentheses":{
			"begin": "\\s*(\\()",
			"beginCaptures": {
				"1": {
					"name": "punctuation.parentheses.left.atlas"
				}
			},
			"end": "(\\))",
			"endCaptures": {
				"1": {
					"name": "punctuation.parentheses.right.atlas"
				}
			},
			"patterns": [
				{"include": "#parentheses"},
				{"include": "#variable"}
			]
		},
		"flags":{
			"patterns": [
				{"include": "#comment"},
				{"include": "#branch"},
				{"inlcude": "#entry"}
			]
		},
		"comment":{
			"name": "comment.block.atlas",
			"begin":"^(C)\\n?[^']",
			"end": "\\$",
			"patterns":[
				{"include": "#comments"},
				{
					"comment": "TPS test number.",
					"contentName": "meta.test-number.atlas",
					"begin": "(?i)\\b(t(\\d{4})|tn(\\d{4}))\\s*?:",
					"beginCaptures": {
						"2": {"name": "constant.statement.test-number.atlas"}
					},
					"end":"(?=\\*\\n)",
					"patterns":[
						{
							"comment": "TPS test number description.",
							"match": "(?i)\\b.*\\b",
							"name": "string.statement.test-number.atlas"
						}
					]
				},
				{
					"contentName": "meta.procedure.atlas",
					"comment": "Procedure meta keyword",
					"begin": "(?i)\\b(procedure\\b(\\s*name)?\\s*?):",
					"beginCaptures": {
						"1": {"name": "string.procedure.atlas"}
					},
					"end": "(?=\\*\\n)",
					"patterns":[
						{
							"comment": "Specific procedure name.",
							"match": "(?i)\\b\\w+\\b",
							"name": "constant.procedure-name.atlas"
						}
					]
				}
			]
		},
		"control-statements": {
			"patterns": [
				{"include": "#if-construct"},
				{"include": "#while-then-construct"},
				{"include": "#for-then-contruct"},
				{"include": "go-to-statement"},
				{"include": "#perform-statement"},
				{"include": "#finish-statement"},
				{"include": "#enable-digital-configuration"},
				{"include": "#disable digital-configuration-statement"},
				{"include": "#escape"}
			]
		},
		"if-construct":{
			"patterns": [
				{
					"begin": "(?i)\\b(if)\\b",
					"beginCaptures": {
						"1": {
							"name": "keyword.control.if.atlas"
						}
					},
					"end": "(?=[;!\\n])",
					"patterns": [
						{
							"contentName": "meta.block.if.atlas",
							"begin": "(?i)\\s*(then)\\s*\\$",
							"beginCaptures": {
								"1": {
									"name": "keyword.control.then.atlas"
								}
							},
							"end": "(?i)\\b(end,\\s*if)\\s*\\$",
							"endCaptures": {
								"1": {
									"name": "keyword.control.endif.atlas"
								}
							},
							"patterns": [
								{
									"comment": "else if statement",
									"begin": "(?i)\\b(else)\\s*\\$",
									"beginCaptures": {
										"1": {
											"name": "keyword.control.else.atlas"
										}
									},
									"end": "(?=[;!\\n])"
								},
								{
									"comment": "else block",
									"begin": "(?i)\\b(else)\\b",
									"beginCaptures": {
										"1": {
											"name": "keyword.control.else.atlas"
										}
									},
									"end": "(?i)(?=\\b(end,\\s*if)\\b)",
									"patterns": [
										{
											"comment": "rest of else line",
											"begin": "(?!(\\s*\\$|\\s*!)|(\\s*\\n))",
											"end": "(?=[;!\\n])"
										},
										{
											"begin": "(?i)(?!\\b(end,\\s*if)\\b)",
											"end": "(?i)(?=\\b(end,\\s*if)\\b)",
											"patterns": [
												{
													"include": "$base"
												}
											]
										}
									]
								},
								{
									"include": "$base"
								}
							]
						},
						{
							"name": "meta.statement.control.if.atlas",
							"begin": "(?i)'(?=\\s*[a-z])'",
							"end": "(?=[;!\\n])",
							"patterns": [
								{
									"include": "$base"
								}
							]
						}
					]
				}
			]
		},
		"operators": {
			"patterns": [
				{"include": "#comparison-operators"},
				{"include": "#unary-operators"},
				{"include": "#pre-defined-functions"}
			]
		},
		"comparison-operators": {
			"match": "(?i)\\b(EQ)\\b|\\b(NE)\\b|\\b(GT)\\b|\\b(LT)\\b|\\b(GE)\\b|\\b(LE)\\b",
			"captures": {
				"1":{"name": "keyword.logical.equal.atlas"},
				"2":{"name": "keyword.logical.notEqual.atlas"},
				"3":{"name": "keyword.logical.greaterThan.atlas"},
				"4":{"name": "keyword.logical.lessThan.atlas"},
				"5":{"name": "keyword.logical.greaterOrEqual.atlas"},
				"6":{"name": "keyword.logical.lessOrEqual.atlas"}
			}
		},
		"unary-operators":{
			"match": "\\s(\\+)\\s|\\s(\\-)\\s|\\s(NOT)\\s",
			"captures":{
				"1":{"name": "keyword.operators.addition.atlas"},
				"2":{"name": "keyword.operators.subtraction.atlas"},
				"3":{"name": "keyword.operators.logicalNot.atlas"}
			}
		}
	},
	"scopeName": "source.atlas"
}